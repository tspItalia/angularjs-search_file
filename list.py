#!/usr/bin/env python
'''
to enable this script you can add this lines to apache config:

<Directory /srv/www/data/files/list.py >
  Options +ExecCGI
  AddHandler cgi-script .py
</Directory>

'''
EXTENSION='.fsc'
from json import JSONEncoder
from os import listdir
from os.path import isfile, join
print("Content-Type: application/json;charset=utf-8")
print("")
mypath="."
onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) and f.endswith(EXTENSION) ]
onlyfiles.sort()
jsoned = JSONEncoder().encode(onlyfiles) 
print( jsoned )

with open("list.json",'w') as fd:
	fd.write(jsoned)