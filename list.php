<?php
  header('Content-Type: application/json;charset=utf-8');
  if ($handle = opendir('.')) {
      $list = new ArrayObject() ;
      while (false !== ($file = readdir($handle))) { 
      if (preg_match('/\.fsc$/',$file) ){
        $list->append($file);
      }
    }
    closedir($handle); 
    echo json_encode($list);
  }
?>